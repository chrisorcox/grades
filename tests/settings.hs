ConfigData { courseName = "discrete"
           , courseNumber = "21-228"
           , fileNames = ["grades", "overrides"]
           , assignments = ["HW1", "HW2", "HW3", "HW4", "Test1", "Midterm"]
           , computed = [ ("Midterm", "$HW1 &HW1 / $HW2 &HW2 / $HW3 &HW3 / $HW4 &HW4 / avg 60 * $Test1 &Test1 / 40 * +") ]
           , letterGrades = [ ("Midterm", [ ("A", 90)
                                        , ("B", 80)
                                        , ("C", 70)
                                        , ("D", 60)
                                        , ("F", 0) ] ) ]
            , dropList = ["cocox"]
            , management = [ ("Chris", "Cox", "cocox@andrew.cmu.edu")
                           , ("Tom", "Bohman", "email" ) ]
            , signature = "--\nBest,\nChris"
            , sendMail = "msmtp -a work -t"
            , sendAtch = "neomutt -s %s -a %s -- '%s'" }
