module GradeLib where

import Data.Default
import Data.List.Split (splitOn)
import qualified Data.Map as M
import Data.Maybe
import qualified Numeric as N
import Text.Printf


type StudentID = String

type Grade = Float -- so we can printf

showGrade :: Grade -> String
showGrade = printf "%.2f"

readGrade :: String -> Grade
readGrade g = case g of
                "" -> 0
                otherwise -> read g

readMaybeGrade :: Maybe String -> Grade
readMaybeGrade g = case g of
                Nothing -> 0
                Just "" -> 0
                Just g' -> read g'


type LetterGrade = String

type Assign = String
data AssignData = AssignData { grade :: Grade
                             , maxGrade :: Grade
                             , letterGrade :: Maybe LetterGrade }
                             deriving (Read, Show)

-- second argument is preferred
updateAssign :: AssignData -> AssignData -> AssignData
updateAssign a b = AssignData { grade = updateUnlessZero (grade a) (grade b)
                              , maxGrade = updateUnlessZero (maxGrade a) (maxGrade b)
                              , letterGrade = updateUnlessNothing (letterGrade a) (letterGrade b) }
                                  where updateUnlessZero x y
                                                    | y == 0 = x
                                                    | otherwise = y
                                        updateUnlessNothing x y
                                                    | y == Nothing = x
                                                    | otherwise = y


type GradeMap = M.Map Assign AssignData

-- again, second argument is preferred
updateGrades :: GradeMap -> GradeMap -> GradeMap
updateGrades = M.unionWith updateAssign



data StudentData = StudentData { firstName :: String
                               , lastName :: String
                               , email :: String
                               , section :: Maybe String
                               , grades :: GradeMap }
                               deriving (Read, Show)

instance Default StudentData where
    def = StudentData { firstName = ""
                      , lastName = ""
                      , email = ""
                      , section = Nothing
                      , grades = M.empty }



-- and yet again, second argument is preferred (for grades, the metadata comes from the first argument)
updateStudent :: StudentData -> StudentData -> StudentData
updateStudent a b = a { grades = updateGrades (grades a) (grades b) }

studentName :: StudentData -> String
studentName s = firstName s ++ " " ++ lastName s

studentContact :: StudentData -> String
studentContact s = studentName s ++ " <" ++ email s ++ ">"


type StudentMap = M.Map StudentID StudentData

mapG :: (AssignData -> AssignData) -> StudentMap -> StudentMap
mapG f sm = M.map f' sm
    where f' s = s { grades = M.map f (grades s) }

mergeStudents :: StudentMap -> StudentMap -> StudentMap
mergeStudents = M.unionWith updateStudent


-- config data

type LetterThreshold = (LetterGrade, Grade)
type Manager = (String, String, String)
            --  ^first  ^last   ^email


data ConfigData = ConfigData { courseName :: String
                             , courseNumber :: String
                             , fileNames :: [FilePath]
                             , assignments :: [Assign]
                             , hidden :: [Assign]
                             , computed :: [(Assign,String)]
                             , letterGrades :: [(Assign,[LetterThreshold])]
                             , dropList :: [StudentID]
                             , management :: [Manager]
                             , signature :: String
                             , sendMail :: String
                             , sendAtch :: String
                             , metaData :: [(String,String)]}
                             deriving (Read, Show)

instance Default ConfigData where
    def = ConfigData { courseName = ""
                     , courseNumber = ""
                     , fileNames = []
                     , assignments = []
                     , hidden = []
                     , computed = []
                     , letterGrades = []
                     , dropList = []
                     , management = [ ("First", "Last", "email") ]
                     , signature = "--\nThe Management"
                     , sendMail = "msmtp -a work -t"
                     , sendAtch = "neomutt -s %s -a %s -- '%s'"
                     , metaData = [ ("first", "Preferred/First Name")
                                  , ("last", "Last Name")
                                  , ("email", "Email")
                                  , ("id", "Student ID")
                                  , ("section", "Section")
                                  ]
                     }

readConfig :: String -> ConfigData
readConfig = read

getConfig :: FilePath -> IO ConfigData
getConfig c = readFile c >>= return . readConfig
-- getConfig c = do
    -- s <- readFile c
    -- return $ readConfig s

managerContact :: Manager -> String
managerContact (a,b,c) = a ++ " " ++ b ++ " <" ++ c ++ ">"

-- usually if first and last names are put together it goes lastname, firstname
fixLastName :: String -> String
fixLastName s = head $ splitOn ", " s

fixFirstName :: String -> String
fixFirstName s = last $ splitOn ", " s


getMaybeMeta :: ConfigData -> [(String,String)] -> Maybe String -> Maybe String
getMaybeMeta cfg stud s = case meta of
                            Just sec -> lookup sec stud
                            Nothing -> Nothing
    where meta = case s of
                   Just s' -> lookup s' $ metaData cfg
                   Nothing -> Nothing

getMeta :: ConfigData -> [(String,String)] -> String -> String
getMeta cfg stud q = fromJust $ lookup meta stud
    where meta = fromJust $ lookup q $ metaData cfg


initStudent :: ConfigData -> [(String,String)] -> StudentData
initStudent cfg stud = StudentData { firstName = fixFirstName $ getIt "first"
                                   , lastName = fixLastName $ getIt "last"
                                   , email = getIt "email"
                                   , section = getMaybeIt (Just "section")
                                   , grades = M.empty }
    where getIt s = getMeta cfg stud s
          getMaybeIt s = getMaybeMeta cfg stud s
