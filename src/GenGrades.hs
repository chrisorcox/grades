module GenGrades where

import Data.List
import Data.List.Split (splitOn)
import Data.Maybe
import qualified Data.Map as M
import System.Process

import GradeLib


genStudentMap :: FilePath -> ConfigData -> IO (StudentMap)
genStudentMap dir cfg = do
    let files = map (dir ++) $ fileNames cfg
    _ <- mapM convertODS files
    processed <- mapM readFromCSV files
    _ <- mapM cleanCSV files
    let students = foldl mergeStudents M.empty $ map (buildStudentMap cfg) processed
    let computedStudents = foldl (\acc a -> computeAssign cfg acc $ fst a) students $ computed cfg
    let letteredStudents = foldl (\acc a -> letterAssign cfg acc $ fst a) computedStudents $ letterGrades cfg
    return $ filterStudents letteredStudents
        where convertODS f = callCommand $ "xlsx2csv " ++ f ++ ".ods --delimiter ';' --replace " ++ f
              -- https://github.com/zitsen/xlsx2csv.rs
              cleanCSV f = callCommand $ "rm " ++ f ++ ".csv"
              readFromCSV :: FilePath -> IO [[(String,String)]]
              readFromCSV f = do
                  csvData <- readFile $ f ++ ".csv"
                  let zipLegend a = map (zip (head a)) $ tail a
                  return $ zipLegend $ map (splitOn ";") . filter (/= []) $ lines csvData
              filterStudents sm = M.filterWithKey (\k _ -> stillHere k) sm
              stillHere k
                | k == "" = False
                | k `elem` (dropList cfg) = False
                | otherwise = True



-- must extract maxes first
parseStudent :: ConfigData -> [(String,String)] -> [(String,String)] -> (StudentID,StudentData)
parseStudent cfg maxes stud = (studid, initialData { grades = initialGrades })
    where studid = getMeta cfg stud "id"
          initialData = initStudent cfg stud
          parseAssignment a = AssignData { grade = readMaybeGrade $ lookup a stud
                                         , maxGrade = readMaybeGrade $ lookup a maxes
                                         , letterGrade = Nothing }
          initialGrades = M.fromList $ map (\a -> (a, parseAssignment a)) $ ((assignments cfg ++ hidden cfg) \\ (map fst $ computed cfg))

buildStudentMap :: ConfigData -> [[(String,String)]] -> StudentMap
buildStudentMap cfg xs = M.fromList $ map (parseStudent cfg maxes) rest
    where (maxes,rest) = filterMax xs

-- we need to get the row containing the maxes...
-- perhaps the best way to do it is to go over every row until the first non-blank and see if it says Max?
filterMax :: [[(String,String)]] -> ( [(String,String)], [[(String,String)]] )
filterMax xs = ( head $ snd splitAtMax , fst splitAtMax ++ (tail $ snd splitAtMax) )
    where splitAtMax = break (hasMax) xs
          hasMax ys = findMax $ map snd ys
          findMax [] = False
          findMax (y:ys)
            | y == "MAX" = True
            | y == "max" = True
            | y == "" = findMax ys
            | otherwise = False


-- compute grades from other grades

computeAssign :: ConfigData -> StudentMap -> Assign -> StudentMap
computeAssign cfg students a = M.map (compStud) students
    where formula = fromJust $ lookup a $ computed cfg
          computeThis gm = AssignData { grade = solveRPN formula gm, maxGrade = calcMaxGrade formula gm, letterGrade = Nothing }
          compStud stud = stud { grades = M.insert a (computeThis $ grades stud) (grades stud) }

-- This is a very simple reverse polish notation solver
solveRPN :: String -> GradeMap -> Grade
solveRPN formula studentGrades = head . foldl parseRPN [] . words $ formula
    where parseRPN (x:y:ys) s
                   | s == "+" = (y + x):ys
                   | s == "-" = (y - x):ys
                   | s == "*" = (y * x):ys
                   | s == "/" = (y / x):ys
                   | s == "^" = (y ** x):ys
                   | s == "max" = (max y x):ys
          parseRPN xs ('$':s) = (getGrade s):xs
          parseRPN xs ('&':s) = (getMaxGrade s):xs
          parseRPN xs ('%':s) = ( (getGrade s) / (getMaxGrade s) ):xs
          parseRPN xs s
                   | s == "sum" = (sum xs):[]
                   | s == "avg" = (avg xs):[]
                   | s == "dropMin" = dropMin xs
          parseRPN xs numberString = (read numberString):xs
          getAssignment a = fromJust $ M.lookup a studentGrades
          getGrade a = grade $ getAssignment a
          getMaxGrade a = maxGrade $ getAssignment a

calcMaxGrade :: String -> GradeMap -> Grade
calcMaxGrade formula = solveRPN (map (\x -> if x=='$' then '&' else x) $ unwords . map fixPercent $ words formula)
    where fixPercent s = case s of
                           ('%':_) -> "1"
                           otherwise -> s



dropMin :: [Float] -> [Float]
dropMin xs = delete m xs
    where m = minimum xs

avg :: [Float] -> Float
avg xs = (sum xs) / l
    where l = fromIntegral $ length xs


-- getAssignment :: Assign -> GradeMap -> AssignData
-- getAssignment a l = fromJust $ M.lookup a l



-- parse letter grades

letterAssign :: ConfigData -> StudentMap -> Assign -> StudentMap
letterAssign cfg students a = M.map (letterStud) students
    where doWeNeed = lookup a $ letterGrades cfg
          findAssign stud = fromJust $ M.lookup a $ grades stud
          withLetter l stud = (a, AssignData { grade = 0, maxGrade = 0, letterGrade = Just $ getLetterGrade l (grade $ findAssign stud) } )
          giveLetter l stud = stud { grades = M.fromList [withLetter l stud] }
          letterStud stud = case doWeNeed of
                              Just lt -> updateStudent stud (giveLetter lt stud)
                              Nothing -> stud




getLetterGrade :: [LetterThreshold] -> Grade -> LetterGrade
getLetterGrade lt g = fst . head . filter p $ lt
    where p (_,x)
            | x <= g = True
            | otherwise = False


