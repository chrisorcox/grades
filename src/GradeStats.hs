module GradeStats where

import Data.List
import qualified Data.Map as M
import Data.Maybe
import qualified Text.PrettyPrint.Boxes as B
import Text.Printf


import GradeLib


data AssignStats = AssignStats { maxPossible :: Grade
                               , gradeDistr :: [Grade]
                               , letterDistr :: [Maybe LetterGrade] }
                               deriving (Read, Show)

type AssignMap = M.Map Assign AssignStats

toAssignStats :: AssignData -> AssignStats
toAssignStats a = AssignStats { maxPossible = maxGrade a
                              , gradeDistr = grade a :[]
                              , letterDistr = letterGrade a :[] }

addAssign :: AssignStats -> AssignStats -> AssignStats
addAssign a b = AssignStats { maxPossible = maxPossible a
                            , gradeDistr = gradeDistr a ++ gradeDistr b
                            , letterDistr = letterDistr a ++ letterDistr b }

-- take the student map, extract GradeData (which is M.Map (Assign,AssignData))
-- Then change these to M.Map (Assign,AssignStats), so we have a list of these.
-- Afterward, we just unionWith addAssign
parseAssignStats :: StudentMap -> AssignMap
parseAssignStats sm = foldr (M.unionWith addAssign) M.empty $ M.elems (M.map extractAssigns sm)
    where extractAssigns :: StudentData -> AssignMap
          extractAssigns stud = M.map toAssignStats $ grades stud


genAssignStats :: StudentMap -> Assign -> AssignStats
genAssignStats stud a = AssignStats { maxPossible = maxGrade $ head assignData
                                    , gradeDistr = map grade assignData
                                    , letterDistr = map letterGrade assignData }
    where gradeData = M.elems $ M.map grades stud
          assignData = map (\gd -> fromJust $ M.lookup a gd) gradeData
          maxGradePoss = maxGrade $ head assignData

getCount :: Ord k => [k] -> M.Map k Int
getCount xs = M.fromListWith (+) $ map (\x -> (x,1)) xs


aveGrade :: AssignStats -> Grade
aveGrade a = (sum xs) / (fromIntegral $ length xs)
    where xs = gradeDistr a


getBins :: AssignStats -> [(String,Int)]
getBins a
  | maxPossible a <= 10 = buildNumBins a
  | otherwise = build100Bins a

buildNumBins :: AssignStats -> [(String,Int)]
buildNumBins a = map (\m -> (show $ round m, countThis m)) $ takeWhile (<= maxPossible a) [0..]
    where countThis m = length $ filter (testBin m) $ gradeDistr a
          testBin m x
            | m == maxPossible a = if x >= m then True else False
            | otherwise = if x >= m && x < m + 1 then True else False


build100Bins :: AssignStats -> [(String,Int)]
build100Bins a = map (\m -> ( (show $ round m) ++ "%" , countThis m)) [0,10..100]
    where countThis m = length $ filter (testBin m) $ gradeDistr a
          perc x = 100 * x / (maxPossible a)
          testBin m x
            | m == 100 = if perc x >= m then True else False
            | otherwise = if perc x >= m && perc x < m + 10 then True else False




padLeft :: Int -> String -> String
padLeft width x = replicate k ' ' ++ x
    where k = width - length x

padRight :: Int -> String -> String
padRight width x = x ++ replicate k ' '
    where k = width - length x

fmtColumnRight :: [String] -> B.Box
fmtColumnRight items = B.vcat B.right . map (B.text . padLeft width) $ items
    where width = maximum $ map length items

fmtColumnLeft :: [String] -> B.Box
fmtColumnLeft items = B.vcat B.left . map (B.text . padRight width) $ items
    where width = maximum $ map length items


gradeTable :: [[String]] -> B.Box
gradeTable rows = B.hcat B.top (lColumn:lsep:gColumns)
    where columns = transpose rows
          nrows = length rows
          -- vsep =  B.vcat B.right $ map B.text (replicate nrows " ")
          lsep = B.vcat B.right $ map B.text (replicate nrows " │ ")
          lColumn = fmtColumnRight $ head columns
          gColumns = map fmtColumnLeft $ tail columns


bin2Hist :: [(String,Int)] -> B.Box
bin2Hist bins = gradeTable newBins
    where newBins = map (\(s,i) -> s:(replicate i "#")) $ reverse bins



printStats :: StudentMap -> Assign -> String
-- printStats stud a = B.render . bin2Hist . getBins $ genAssignStats stud a
printStats s a = B.render . bin2Hist . getBins . fromJust $ M.lookup a (parseAssignStats s)

