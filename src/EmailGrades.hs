module EmailGrades where

import Data.List
import Data.Maybe
import qualified Data.Map as M
import qualified Text.PrettyPrint.Boxes as B
import Text.Printf
-- pretty print grades


import GradeLib

type Message = Maybe String
type Subject = Maybe String

padLeft :: Int -> String -> String
padLeft width x = replicate k ' ' ++ x
    where k = width - length x

padRight :: Int -> String -> String
padRight width x = x ++ replicate k ' '
    where k = width - length x

fmtColumnRight :: [String] -> B.Box
fmtColumnRight items = B.vcat B.right . map (B.text . padLeft width) $ items
    where width = maximum $ map length items

fmtColumnLeft :: [String] -> B.Box
fmtColumnLeft items = B.vcat B.left . map (B.text . padRight width) $ items
    where width = maximum $ map length items


gradeTable :: [[String]] -> B.Box
gradeTable rows = B.hcat B.top (lColumn:lsep:gColumns)
    where columns = transpose rows
          nrows = length rows
          vsep =  B.vcat B.right $ map B.text (replicate nrows "  ")
          lsep = B.vcat B.right $ map B.text (replicate nrows " │ ")
          lColumn = fmtColumnLeft $ head columns
          gColumns = intersperse vsep . map fmtColumnRight $ tail columns

sortGrades :: ConfigData -> GradeMap -> [(Assign, AssignData)]
sortGrades cfg as = foldr (\x acc -> ((x,findMe x)):acc) [] $ assignments cfg
    where findMe x = fromJust . M.lookup x $ as

formatGrade :: (Assign, AssignData) -> [String]
formatGrade (a, g) = [ a, gg, "out of", gm, gl ]
    where gg = showGrade $ grade g
          gm = showGrade $ maxGrade g
          gl = case letterGrade g of
                 Just letter -> "(Letter grade is " ++ letter ++ ")"
                 Nothing -> ""

emailAtchHead :: ConfigData -> Subject -> [FilePath] -> StudentData -> String
emailAtchHead cfg sbj files s = printf (sendAtch cfg) (subjectString sbj) (unwords files) (studentContact s)
                             ++ " <<EOF\n"
    where subjectString sub = "'[" ++ courseNumber cfg ++ "] " ++ sub' ++ "'"
                where sub' = case sub of
                              Just subject -> subject
                              Nothing -> "Grades"

emailHeader :: ConfigData -> Subject -> StudentData -> String
emailHeader cfg sbj s = sendMail cfg ++ " <<EOF\n"
                     ++ "From: " ++ (managerContact . head $ management cfg) ++ "\n"
                     ++ "To: " ++ studentContact s ++ "\n"
                     ++ "Subject: " ++ (subjectString sbj) ++ "\n"
    where subjectString sub = "[" ++ courseNumber cfg ++ "] " ++ sub'
              where sub' = case sub of
                             Just subject -> subject
                             Nothing -> "Grades"

emailBody :: ConfigData -> Bool -> Message -> StudentData -> String
emailBody cfg incl msg s = "Dear " ++ first ++ ",\n\n"
               ++ msgIfExists
               ++ (gradeReport incl)
               ++ (signature cfg) ++ "\n"
    where first = firstName s
          msgIfExists = case msg of
                          Just m -> m ++ "\n\n"
                          Nothing -> ""
          gradeReport b | b == True = "Your grade" ++ (isAre s) ++ " as follows:\n\n"
                                   ++ (B.render . gradeTable . map formatGrade $ sortGrades cfg $ grades s) ++ "\n"
                                   ++ "Let me know as soon as possible if there is a mistake.\n\n"
                        | b == False = ""
                        where isAre y | num > 1 = "s are"
                                      | otherwise = " is"
                                      where num = length $ grades y

emailFooter :: StudentData -> String
emailFooter s = "EOF\necho 'Email sent to " ++ studentName s ++ "'\n"

genEmail cfg incl sbj msg s = (emailHeader cfg sbj s) ++ (emailBody cfg incl msg s) ++ emailFooter s

genEmails :: ConfigData -> Bool -> Subject -> Message -> Maybe [FilePath] -> [StudentData] -> String
genEmails cfg incl sbj msg files ss = "#!/bin/sh\n\n" ++ (unlines . map buildFunc $ ss) ++ "echo 'Students have been enlightened...'"
    where buildFunc s = (pickHeader s) ++ (emailBody cfg incl msg s) ++ (emailFooter s)
          pickHeader s = case files of
                         Just fs -> emailAtchHead cfg sbj fs s
                         Nothing -> emailHeader cfg sbj s
    -- where buildFunc = genEmail cfg incl sbj msg
