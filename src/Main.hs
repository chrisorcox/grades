module Main where

import Control.Applicative
import Data.Default
import qualified Data.Map as M
import Data.Monoid ((<>))
import qualified Filesystem.Path.CurrentOS as F
import Options.Applicative

import GradeLib
import EmailGrades (genEmails)
import GenGrades (genStudentMap)
import GradeStats (printStats)

data MainOptions = MainOptions { configFile :: !FilePath, optCmd :: !Command }
mainOptions :: Parser MainOptions
mainOptions = MainOptions
          <$> strOption
              ( long "config"
             <> short 'c'
             <> metavar "<FILE>"
             <> value "./config.hs"
             <> help "Config file" )
          <*> hsubparser
              ( command "mail"
                ( info mailOptions (progDesc "Generate student emails") )
             <> command "parse"
                ( info parseOptions (progDesc "Parse spreadsheets") )
             <> command "stats"
                ( info statsOptions (progDesc "View statistics") ) )


data Command = MailOptions { message :: String
                           , subject :: String
                           , altEmail :: Bool
                           , attachment :: String
                           , section :: String }
                | ParseOptions { initialize :: Bool }
                | StatsOptions { listAll :: Bool
                               , statSec :: String
                               , forAssignment :: String}
                deriving (Show)

mailOptions :: Parser Command
mailOptions = MailOptions
          <$> strOption
              ( long "message"
             <> short 'm'
             <> metavar "<TEXT>"
             <> value ""
             <> help "Message to include in emails" )
          <*> strOption
              ( long "subject"
             <> short 's'
             <> metavar "<TEXT>"
             <> value "Grades"
             <> help "Subject for the emails" )
          <*> switch
              ( long "alternate"
             <> short 'A'
             <> help "Send an email other than grades" )
          <*> strOption
              ( long "attachment"
             <> short 'a'
             <> value ""
             <> metavar "<FILE> [<FILE2>...]"
             <> help "Attach file to emails" )
          <*> strOption
              ( long "section"
             <> short 'S'
             <> value ""
             <> metavar "<TEXT>"
             <> help "Email only specified sections" )

parseOptions :: Parser Command
parseOptions = ParseOptions
           <$> switch
               ( long "init"
              <> short 'i'
              <> help "Initialize the directory" )

statsOptions :: Parser Command
statsOptions = StatsOptions
           <$> switch
               ( long "all"
              <> short 'A'
              <> help "Show all grades as opposed to just the statistics summary" )
           <*> strOption
               ( long "section"
              <> short 'S'
              <> value ""
              <> metavar "<TEXT>"
              <> help "Show statistics for only specified sections" )
           <*> strOption
               ( long "assignment"
              <> short 'a'
              <> value ""
              <> metavar "<TEXT>"
              <> help "Show statistics for only specified assignments" )


main :: IO ()
main = do opts <- execParser myOpts
          let configLocation = configFile opts
          let workingDir = F.encodeString . F.directory $ F.decodeString configLocation
          config <- getConfig configLocation
          case optCmd opts of
            MailOptions m s a aa ss -> sendEmail workingDir config m s a aa ss
            ParseOptions i -> parseGrades workingDir config i
            StatsOptions a s aa -> showStats workingDir config a s aa
    where myOpts = info (helper <*> mainOptions)
            ( fullDesc
           <> progDesc "Process student grades and send emails" )

readStudents :: String -> StudentMap
readStudents = read

sendEmail :: FilePath -> ConfigData -> String -> String -> Bool -> String -> String -> IO ()
sendEmail dir cfg msg sbj alt atch sec = do students <- (readFile $ dir ++ ".data/student.map") >>= return . readStudents
                                            putStrLn $ genEmails cfg (not alt) sbj' msg' atch' $ map snd $ M.toList students
    where sbj' = case sbj of
                   "" -> Nothing
                   otherwise -> Just sbj
          msg' = case msg of
                   "" -> Nothing
                   otherwise -> Just msg
          atch' = case atch of
                    "" -> Nothing
                    otherwise -> Just $ words atch

parseGrades :: FilePath -> ConfigData -> Bool -> IO ()
-- parseGrades dir cfg i = do students <- genStudentMap dir cfg
                           -- writeFile (dir ++ ".data/student.map") $ show students
parseGrades dir cfg i = case i of
                           True -> putStrLn $ show (def :: ConfigData)
                           False -> do students <- genStudentMap dir cfg
                                       writeFile (dir ++ ".data/student.map") $ show students

showStats :: FilePath -> ConfigData -> Bool -> String -> String -> IO ()
-- showStats cfg a s aa = putStrLn "Coming soon to a cli near you!"
showStats dir cfg a s aa = do students <- (readFile $ dir ++ ".data/student.map") >>= return . readStudents
                              putStrLn $ foldl (\acc a -> acc ++ a ++ "\n" ++ (printStats students a) ++ "\n") "" $ assignments cfg
