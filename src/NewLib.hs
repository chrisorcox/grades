module Letters where

-- import Data.List.Split (splitOn)
-- import qualified Data.Map as M
-- import Data.Maybe
-- import qualified Numeric as N
-- import Text.Printf


data Letter = F | R | Dm | D | Dp | Cm | C | Cp | Bm | B | Bp | Am | A | Ap deriving (Eq, Ord)

type LetterGrade = Maybe Letter

instance Show Letter where
    show Ap = "A+"
    show A = "A"
    show Am = "A-"
    show Bp = "B+"
    show B = "B"
    show Bm = "B-"
    show Cp = "C+"
    show C = "C"
    show Cm = "C-"
    show Dp = "D+"
    show D = "D"
    show Dm = "D-"
    show R = "R"
    show F = "F"
